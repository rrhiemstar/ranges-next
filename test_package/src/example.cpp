#include <iostream>
#include "next/next.hpp"

using namespace next;

int main() {

	for (auto i : linrange(1, 10) |
		filter([](int i) { return i % 2 == 0; }) |
		transform([](int i) { return i + 1; }) |
		take(3))
	{
		std::cout << i << std::endl;
	}

	return 0;
}