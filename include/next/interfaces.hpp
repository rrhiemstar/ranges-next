#pragma once

#include <concepts>
#include <iterator> // for iterator_traits, begin, end
#include <optional>
#include <utility>  // for forward, move


#include "meta.hpp"

using namespace meta;

namespace next {

#pragma region next interface

// This interface class is used in CRTP
template <typename Range> struct next_interface : public crtp<Range> {

  struct iterator {
    using T = typename Range::value_type;

    Range &range;
    std::optional<T> current;

    iterator(Range &range, std::optional<T> value)
        : range(range), current(value) {}

    iterator &operator++() {
      current = range.next();
      return *this;
    }

    bool operator==(iterator const &other) const {
      return current == other.current;
    }

    bool operator!=(iterator const &other) const { return !(*this == other); }

    T const &operator*() const { return *current; }

    T const *operator->() const { return std::addressof(*current); }
  };

  constexpr iterator begin() {
    this->underlying().initialize();
    return {this->underlying(), std::optional(this->underlying().next())};
  }

  constexpr iterator end() { return {this->underlying(), std::nullopt}; }
};

template <typename T>
concept InputRange = std::derived_from<
    std::remove_reference_t<T>, next_interface<std::remove_reference_t<T>>> &&
    requires(T t) {
  t.initialize();
  t.next();
};

template <typename R>
concept Iterable = requires(R r) {
  std::begin(r);
  std::end(r);
};

template <InputRange Range> using value_t = typename Range::value_type;

#pragma endregion

} // namespace next