#pragma once

#include <tuple>
#include <concepts>

#include "interfaces.hpp"


namespace next {

#pragma region filtered range

// Range which filters elements of another range, based on a predicate
template <InputRange Range, Predicate Pred>
struct filtered_range : next_interface<filtered_range<Range, Pred>> {
  Range range;
  const Pred pred;

  using type = filtered_range<Range, Pred>;
  using value_type = typename Range::value_type;

  filtered_range(Range range, const Pred &pred)
      : range(std::move(range)), pred(pred) {}

  void initialize() { range.initialize(); }

  std::optional<value_type> next() {
    while (const auto value = range.next())
      if (pred(*value))
        return value;
    return std::nullopt;
  }
};

template <InputRange Range, Predicate Pred>
filtered_range(Range, Pred) -> filtered_range<Range, Pred>;

template <Predicate Pred> struct filter_proxy { Pred pred; };

template <typename Pred> auto filter(Pred &&pred) {
  return filter_proxy<Pred>{std::forward<Pred>(pred)};
}

template <InputRange Range, Predicate Pred>
auto operator|(Range &&range, filter_proxy<Pred> proxy) {
  return filtered_range{std::forward<Range>(range), std::move(proxy.pred)};
}

#pragma endregion

#pragma region take from range

// Range which filters elements of another range, based on a predicate
template <InputRange Range>
struct take_range : next_interface<take_range<Range>> {
  Range range;
  std::size_t take;

  using type = take_range<Range>;
  using value_type = typename Range::value_type;

  take_range(Range r, std::size_t t) : range(std::move(r)), take(t) {}

  void initialize() { range.initialize(); }

  std::optional<value_type> next() {
    if (take > 0) {
      --take;
      return range.next();
    }
    return std::nullopt;
  }
};

template <typename Range> take_range(Range) -> take_range<Range>;

struct take_proxy {
  std::size_t take;
};

auto take(std::size_t take) { return take_proxy{take}; }

template <InputRange Range> auto operator|(Range &&range, take_proxy proxy) {
  return take_range{std::forward<Range>(range), proxy.take};
}

#pragma endregion

#pragma region drop from range

// Range which filters elements of another range, based on a predicate
template <InputRange Range>
struct drop_range : next_interface<drop_range<Range>> {
  Range range;
  std::size_t drop;

  using type = drop_range<Range>;
  using value_type = value_t<Range>;

  drop_range(Range r, std::size_t d) : range(std::move(r)), drop(d) {}

  void initialize() { range.initialize(); }

  std::optional<value_type> next() {
    while (drop > 0) {
      range.next();
      --drop;
    }

    return range.next();
  }
};

template <typename Range> drop_range(Range) -> drop_range<Range>;

struct drop_proxy {
  std::size_t drop;
};

auto drop(std::size_t drop) { return drop_proxy{drop}; }

template <InputRange Range> auto operator|(Range &&range, drop_proxy proxy) {
  return drop_range{std::forward<Range>(range), proxy.drop};
}

#pragma endregion

#pragma region take while

// Range which filters elements of another range, based on a predicate
template <InputRange Range, Predicate Pred>
struct take_while_range : next_interface<take_while_range<Range, Pred>> {
  Range range;
  Pred pred;
  bool toggle = true;

  using type = take_while_range<Range, Pred>;
  using value_type = typename Range::value_type;

  take_while_range(Range r, const Pred &p) : range(std::move(r)), pred(p) {}

  void initialize() { range.initialize(); }

  std::optional<value_type> next() {
    auto value = range.next();
    if (pred(*value))
      return value;
    return std::nullopt;
  }
};

template <typename Range, Predicate Pred>
take_while_range(Range, Pred) -> take_while_range<Range, Pred>;

template <Predicate Pred> struct take_while_proxy { Pred pred; };

template <Predicate Pred> auto take_while(Pred &&pred) {
  return take_while_proxy<Pred>{std::forward<Pred>(pred)};
}

template <InputRange Range, Predicate Pred>
auto operator|(Range &&range, take_while_proxy<Pred> proxy) {
  return take_while_range{std::forward<Range>(range), std::move(proxy.pred)};
}

#pragma endregion

#pragma region drop while

// Range which filters elements of another range, based on a predicate
template <InputRange Range, Predicate Pred>
struct drop_while_range : next_interface<drop_while_range<Range, Pred>> {
  Range range;
  Pred pred;
  bool toggle = true;

  using type = drop_while_range<Range, Pred>;
  using value_type = typename Range::value_type;

  drop_while_range(Range r, const Pred &p) : range(std::move(r)), pred(p) {}

  void initialize() { range.initialize(); }

  std::optional<value_type> next() {
    auto value = range.next();
    if (toggle) {
      while (pred(*value)) {
        value = range.next();
      }
      toggle = false;
    }
    return value;
  }
};

template <typename Range, Predicate Pred>
drop_while_range(Range, Pred) -> drop_while_range<Range, Pred>;

template <Predicate Pred> struct drop_while_proxy { Pred pred; };

template <typename Pred> auto drop_while(Pred &&pred) {
  return drop_while_proxy<Pred>{std::forward<Pred>(pred)};
}

template <InputRange Range, Predicate Pred>
auto operator|(Range &&range, drop_while_proxy<Pred> proxy) {
  return drop_while_range{std::forward<Range>(range), std::move(proxy.pred)};
}

#pragma endregion

#pragma region transformed range

// Range which applies a transform to another range
template <InputRange Range, Callable Func>
struct transformed_range : next_interface<transformed_range<Range, Func>> {
  Range range;
  const Func func;

  using type = transformed_range<Range, Func>;
  using value_type = decltype(func(*range.next()));

  transformed_range(Range r, const Func &f) : range(std::move(r)), func(f) {}

  void initialize() { range.initialize(); }

  std::optional<value_type> next() {
    if (const auto value = range.next())
      return func(*value);
    else
      return std::nullopt;
  }
};

template <InputRange Range, Callable Func>
transformed_range(Range, Func) -> transformed_range<Range, Func>;

// Pipe-syntax enabler structs and operator overloads
template <Callable Func> struct transform_proxy { Func func; };

template <typename Func> auto transform(Func &&func) {
  return transform_proxy<Func>{std::forward<Func>(func)};
}

template <InputRange Range, Callable Func>
auto operator|(Range &&range, transform_proxy<Func> proxy) {
  return transformed_range{std::forward<Range>(range), std::move(proxy.func)};
}

#pragma endregion

#pragma region enumerate

// Range which filters elements of another range, based on a predicate
template <InputRange Range>
struct enumerate : next_interface<enumerate<Range>> {
  Range &range;
  std::size_t count = 0;

  using type = enumerate<Range>;
  using value_type = std::pair<std::size_t, typename Range::value_type>;

  enumerate(Range &r) : range(r) {}
  enumerate(Range &&r) : range(r) {}

  void initialize() {
    range.initialize();
    count = 0;
  }

  std::optional<value_type> next() {
    auto next = range.next();
    if (next.has_value())
      return value_type{count++, *next};

    return std::nullopt;
  }
};

#pragma endregion

} // namespace next