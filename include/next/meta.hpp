#pragma once

#include <concepts>
#include <tuple>
#include <string_view>

namespace meta {

#pragma region type_name

    template <typename T>
    constexpr auto type_name() noexcept {
        std::string_view name, prefix, suffix;
#ifdef __clang__
        name = __PRETTY_FUNCTION__;
        prefix = "auto type_name() [T = ";
        suffix = "]";
#elif defined(__GNUC__)
        name = __PRETTY_FUNCTION__;
        prefix = "constexpr auto type_name() [with T = ";
        suffix = "]";
#elif defined(_MSC_VER)
        name = __FUNCSIG__;
        prefix = "auto __cdecl type_name<";
        suffix = ">(void) noexcept";
#endif
        name.remove_prefix(prefix.size());
        name.remove_suffix(suffix.size());
        return name;
    }

#pragma endregion

#pragma region Helper functions for CRTP

    template <typename T> struct crtp {
      T &underlying() { return static_cast<T &>(*this); }
      T const &underlying() const { return static_cast<T const &>(*this); }
    };

#pragma endregion

#pragma region Concept Function, Function_object, callable

    // std::invoke<F,Args...> is a way to check if F is a Function or Function-like object. However
    // it requires 'Args...', which may not always be available. 'callable<F>' is a concept that identifies
    // if 'F' is a regular Function, lambda, or Function object.

    template<typename F>
    concept FunctionObject = requires (F) {
        &F::operator();
    };

    template<typename F>
    struct is_function_trait : public std::false_type { };

    template<typename Ret, typename... Args>
    struct is_function_trait<Ret(Args...)> : public std::true_type { };

    template<typename F>
    concept Function = is_function_trait<F>::value;

    template<typename F>
    concept Callable = Function<F> || FunctionObject<F>;

#pragma endregion

#pragma region Concept predicate

    // std::predicate<F,Args> requires both Function type and argument types. The latter is redundent.
    // predicate<F> operates solely on F and works as expected in case 'F' is a regular Function, lambda
    // or Function object

    template<typename F>
    struct is_predicate_trait : public std::false_type { };

    template<typename... Args>
    struct is_predicate_trait<bool(Args...)> : public std::true_type {};

    template<typename F, typename... Args>
    struct is_predicate_trait<bool(F::*)(Args...)> : public std::true_type { };

    template<typename F, typename... Args>
    struct is_predicate_trait<bool(F::*)(Args...) const> : public std::true_type { };

    template<FunctionObject F>
    struct is_predicate_trait<F> : is_predicate_trait<decltype(&F::operator())> { };

    template<typename F>
    concept Predicate = is_predicate_trait<F>::value;

#pragma endregion

#pragma region Function signature introspection

    template<typename Ret, typename... Args>
    struct SignatureInterface {
        static constexpr std::size_t nargs = sizeof...(Args);

        using return_arg_t = Ret;
        using input_arg_t = std::tuple<Args...>;
    };

    // primary template - should never be instantiated
    template<typename F>
    struct Signature : SignatureInterface<void,void> {
    };

    template<typename Ret, typename... Args>
    struct Signature<Ret(Args...)> : public SignatureInterface<Ret, Args...> {};

    template<typename Ret, typename F, typename... Args>
    struct Signature<Ret(F::*)(Args...)> : public SignatureInterface<Ret, Args...> {};

    template<typename Ret, typename F, typename... Args>
    struct Signature<Ret(F::*)(Args...) const> : public SignatureInterface<Ret, Args...> {};

    template<FunctionObject F>
    struct Signature<F> : public Signature<decltype(&F::operator())> {};

#pragma endregion

}



















// #pragma region meta type
//
// // extract the N'th argument type of a parameter pack
// template<size_t N, typename... Ts>
// using argument_t = typename std::tuple_element<N, std::tuple<Ts...>>::type;
//
// // extract the first argument type of a parameter pack
// template<typename... Ts >
// using first_argument_t = argument_t<0, Ts...>;
//
// // extract the last argument type of a parameter pack
// template<typename... Ts >
// using last_argument_t = argument_t<sizeof...(Ts) - 1, Ts...>;
//
// // extract the N'th argument of a parameter pack
// template <int I, class... Ts>
// argument_t<I, Ts...> get(Ts&&... ts) {
// 	return std::get<I>(std::forward_as_tuple(ts...));
// }
//
// // extract the first argument of a parameter pack
// template <class... Ts>
// first_argument_t<Ts...> first(Ts&&... ts) {
// 	return std::get<0>(std::forward_as_tuple(ts...));
// }
//
// // extract the last argument of a parameter pack
// template <class... Ts>
// last_argument_t<Ts...> last(Ts&&... ts) {
// 	return std::get<(sizeof...(Ts) - 1)>(std::forward_as_tuple(ts...));
// }
//
// #pragma endregion