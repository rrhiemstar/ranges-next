#pragma once

#include "interfaces.hpp"
#include <vector>
#include <concepts>
#include <optional>



namespace next {

#pragma region convert range to vector

template <typename Range> auto to_vector_imp(Range &&range) {
  using T = std::remove_reference_t<Range>::value_type;
  std::vector<T> result;
  for (const auto &value : range) {
    result.push_back(value);
  }
  return result;
}

struct to_vector_proxy {};

auto to_vector() { return to_vector_proxy(); }

template <typename Range> auto to_vector(Range &&range) {
  return to_vector_imp(std::forward<Range>(range));
}

template <typename Range> auto operator|(Range &&range, to_vector_proxy) {
  return to_vector_imp(std::forward<Range>(range));
}

#pragma endregion

#pragma region input_range

// Non-owning input view based on STL iterators
template <typename InputIt, typename Sentinel>
struct input_range : next_interface<input_range<InputIt, Sentinel>> {
  InputIt first;
  Sentinel last;
  InputIt current;

  input_range(InputIt first, Sentinel last)
      : first(first), last(last), current(first) {}

  using type = input_range<InputIt, Sentinel>;
  using value_type = typename std::iterator_traits<InputIt>::value_type;

  void initialize() { current = first; }

  std::optional<value_type> next() {
    if (current != last)
      return *current++;
    else
      return std::nullopt;
  }
};

template <typename InputIt, typename Sentinel>
input_range(InputIt, Sentinel) -> input_range<InputIt, Sentinel>;

// Factory function taking anything with begin/end support and returning
// a mutable view
template <Iterable Container> auto make_range(Container &&container) {
  return input_range{std::begin(container), std::end(container)};
}

template <Iterable Container, typename Proxy>
auto operator|(Container &&container, Proxy &&proxy) {
  return operator|(make_range(container), proxy);
}

#pragma endregion

} // namespace next