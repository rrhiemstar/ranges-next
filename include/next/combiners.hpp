#pragma once

#include <tuple>

#include "interfaces.hpp"


namespace next {

#pragma region join range

namespace details {
template <std::size_t M, typename Tuple, typename T> struct join_helper {
  static constexpr std::size_t N = std::tuple_size<Tuple>::value;

  static void initialize(Tuple &ranges) {
    std::get<M>(ranges).initialize();
    if constexpr (M < N - 1)
      join_helper<M + 1, Tuple, T>::initialize(ranges);
  }

  static std::optional<T> next_value(Tuple &ranges) {
    if (const auto v = std::get<M>(ranges).next())
      return v;

    if constexpr (M < N - 1)
      return join_helper<M + 1, Tuple, T>::next_value(ranges);

    return std::nullopt;
  }
};

} // namespace details

template <typename... Ranges> struct join : next_interface<join<Ranges...>> {

  static constexpr std::size_t N = sizeof...(Ranges);

  using type = join<Ranges...>;
  using range_type = std::tuple<Ranges &...>;
  using value_type =
      typename std::common_type<typename Ranges::value_type...>::type;

  range_type ranges;

  join(Ranges &...r) : ranges(r...) {}
  join(Ranges &&...r) : ranges(r...) {}

  void initialize() {
    details::join_helper<0, range_type, value_type>::initialize(ranges);
  }

  std::optional<value_type> next() {
    return details::join_helper<0, range_type, value_type>::next_value(ranges);
  }
};

#pragma endregion

#pragma region zip range

namespace details {
template <std::size_t M, typename Tuple, typename T> struct zip_helper {
  static constexpr std::size_t N = std::tuple_size<Tuple>::value;

  static void initialize(Tuple &ranges) {
    std::get<M>(ranges).initialize();
    if constexpr (M < N - 1)
      zip_helper<M + 1, Tuple, T>::initialize(ranges);
  }

  static std::optional<T> next_value(Tuple &ranges, T &tuple) {
    if (const auto v = std::get<M>(ranges).next()) {
      std::get<M>(tuple) = *v;

      if constexpr (M < N - 1)
        return zip_helper<M + 1, Tuple, T>::next_value(ranges, tuple);

      if constexpr (M == N - 1) {
        return {tuple};
      }
    }
    return std::nullopt;
  }
};

}; // namespace details

// Range which applies a transform to another range
template <typename... Ranges> struct zip : next_interface<zip<Ranges...>> {

  static constexpr std::size_t N = sizeof...(Ranges);

  using type = zip<Ranges...>;
  using range_type = std::tuple<Ranges &...>;
  using value_type = std::tuple<typename Ranges::value_type...>;

  range_type ranges;
  value_type tuple;

  zip(Ranges &...r) : ranges(r...) {}
  zip(Ranges &&...r) : ranges(r...) {}

  void initialize() {
    details::zip_helper<0, range_type, value_type>::initialize(ranges);
  }

  std::optional<value_type> next() {
    return details::zip_helper<0, range_type, value_type>::next_value(ranges,
                                                                      tuple);
  }
};

#pragma endregion

#pragma region product range

namespace details {
// recursive step
template <std::size_t M, std::size_t N, typename Tuple, typename T>
struct product_helper {
  static void initialize(Tuple &ranges, T &tuple) {
    std::get<M>(ranges).initialize();
    std::get<M>(tuple) = *std::get<M>(ranges).next();
    product_helper<M + 1, N, Tuple, T>::initialize(ranges, tuple);
  }

  static std::optional<T> next_value(Tuple &ranges, T &tuple) {
    if (const auto v = std::get<M>(ranges).next()) {
      std::get<M>(tuple) = *v;
      return product_helper<0, N, Tuple, T>::next_value(ranges, tuple);
    } else {
      std::get<M>(ranges).initialize();
      if (const auto v = std::get<M>(ranges).next()) {
        std::get<M>(tuple) = *v;
      }
      return product_helper<M + 1, N, Tuple, T>::next_value(ranges, tuple);
    }
  }
};

// start condition
template <std::size_t N, typename Tuple, typename T>
struct product_helper<0, N, Tuple, T> {
  static void initialize(Tuple &ranges, T &tuple) {
    std::get<0>(ranges).initialize();
    product_helper<1, N, Tuple, T>::initialize(ranges, tuple);
  }

  static std::optional<T> next_value(Tuple &ranges, T &tuple) {
    if (const auto v = std::get<0>(ranges).next()) {
      std::get<0>(tuple) = *v;
      return {tuple};
    } else {
      std::get<0>(ranges).initialize();
      return product_helper<1, N, Tuple, T>::next_value(ranges, tuple);
    }
  }
};

// stop condition
template <std::size_t N, typename Tuple, typename T>
struct product_helper<N, N, Tuple, T> {
  static void initialize(Tuple &ranges, T &tuple) {
    std::get<N>(ranges).initialize();
    std::get<N>(tuple) = *std::get<N>(ranges).next();
  }

  static std::optional<T> next_value(Tuple &ranges, T &tuple) {
    if (const auto v = std::get<N>(ranges).next()) {
      std::get<N>(tuple) = *v;
      return product_helper<0, N, Tuple, T>::next_value(ranges, tuple);
    } else {
      return std::nullopt;
    }
  }
};

} // namespace details

// Range which applies a transform to another range
template <typename... Ranges>
struct product : next_interface<product<Ranges...>> {

  static constexpr std::size_t N = sizeof...(Ranges);

  using type = product<Ranges...>;
  using range_type = std::tuple<Ranges &...>;
  using value_type = std::tuple<typename Ranges::value_type...>;

  range_type ranges;
  value_type tuple;

  product(Ranges &...r) : ranges(r...) {}
  product(Ranges &&...r) : ranges(r...) {}

  void initialize() {
    details::product_helper<0, N - 1, range_type, value_type>::initialize(
        ranges, tuple);
  }

  std::optional<value_type> next() {
    return details::product_helper<0, N - 1, range_type,
                                   value_type>::next_value(ranges, tuple);
  }
};

#pragma endregion

} // namespace next