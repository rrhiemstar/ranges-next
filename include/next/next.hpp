#pragma once

#include "adapters.hpp"
#include "combiners.hpp"
#include "ranges.hpp"
#include "collections.hpp"