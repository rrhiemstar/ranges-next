#pragma once

#include "interfaces.hpp"

namespace next {

#pragma region linear range

template <typename T> struct linrange : next_interface<linrange<T>> {

  const T first;
  const T last;
  T current;

  linrange(const T &first, const T &last)
      : first(first), last(last), current(first) {}

  using type = linrange<T>;
  using value_type = T;

  void initialize() { current = first; }

  std::optional<value_type> next() {
    if (current != last)
      return current++;
    else
      return std::nullopt;
  }
};

#pragma endregion

} // namespace next