# range-next

[![pipeline status](https://gitlab.com/rrhiemstar/ranges-next/badges/master/pipeline.svg)](https://gitlab.com/rrhiemstar/ranges-next/-/commits/master)
[![coverage report](https://gitlab.com/rrhiemstar/ranges-next/badges/master/coverage.svg)](https://rrhiemstar.gitlab.io/ranges-next/)

With c++20 we finally have [ranges](https://en.cppreference.com/w/cpp/ranges) in the standard. Although [std::ranges](https://en.cppreference.com/w/cpp/ranges) is a powerful library there may be some reasons why you may want to consider an alternative, such as [rangeless](https://github.com/ast-al/rangeless), [pipes](https://github.com/joboccara/pipes), or [range-next](https://gitlab.com/rrhiemstar/ranges-next/):
* [std::ranges](https://en.cppreference.com/w/cpp/ranges) may have a large compilation overhead.
* [std::ranges](https://en.cppreference.com/w/cpp/ranges) range-adapters have some operations that can be avoided, see [fluentcpp](https://www.fluentcpp.com/2019/02/15/how-smart-output-iterators-fare-with-the-terrible-problem-of-incrementing-a-smart-iterator/)
* ranges as rvalue references are not supported.
* custom ranges are difficult to implement.

This last point is the "Raison d'être" of this library. It is lightweight and provides a small set of procedures that make it very simple to write your own custom ranges. The design of this library is based on an idea of Vincent Zalzal posted on [fluentcpp](https://www.fluentcpp.com/2019/04/16/an-alternative-design-to-iterators-and-ranges-using-stdoptional/). An additional set of procedures make these type of ranges compatible with "foreach" and, in the future, with "algorithms".

## Example composable ranges
Similar to [std::ranges](https://en.cppreference.com/w/cpp/ranges) several range adapters and a piping command are provided. The adapted ranges are composable and can be used in a ```foreach``` loop as follows
```cpp
int main() {

	for (auto i : linrange(1, 10) |
			filter([](int i) { return i % 2 == 0; }) |
				transform([](int i) { return i + 1; }) |
					take(3))
	{
		std::cout << i << std::endl;
	}
	
    return 0;
}
```
```
output:
3
5
7
```
Here `linrange` yields a linearly increasing range from 1 up to and including 9, the `filter` applies a predicate returning even numbers and `transform` adds one. Note that in contrast to [std::ranges](https://en.cppreference.com/w/cpp/ranges) rvalue references are allowed as input.

## Range adapters
A range adapter takes in a range and outputs an adapted range, usually based on a predicate or a function transformation. [range-next]() implements the following range adapters:
* take
* drop
* take_while
* drop_while
* filter
* transformation
* enumerate

All of these range adapters are standard except for `enumerate`, which appends the input range with a counter for convenience, viz
```cpp
int main() {

	for (auto [i, v] : enumerate(linrange(1, 4)))
	{
		std::cout << i << "  " << v << std::endl;
	}
	
    return 0;
}
```
```
output:
0  1
1  2
2  3
```

## Input and output to std containers
It's is possible to use a `std::vector` or another container that implements `std::begin(container)` and `std::end(container)` as an input range. For example,
```cpp
int main() {

  std::vector<int> v{1, 2, 3, 4}; // create a vector

  for (auto value : v | filter([](int i) { return i % 2 == 0; })) {
    std::cout << value << std::endl;
  }

  return 0;
}
```
```
output:
2
4
```

It's also possible to output the result of a range adapter to `std::vector`. Here is an example,
```cpp
int main() {

	auto v = linrange(1, 10) 
				| filter([](int i) { return i % 2 == 0; }) 
				| transform([](int i) { return i + 1; }) 
				| take(3)
				| to_vector();

	std::cout << "Length of vector: " << v.size() << std::endl;

	for (auto value : v) 
	{
		std::cout << value << std::endl;
	}
	
    return 0;
}
```
```
output:
Length of vector: 3
3
5
7
```
In the future other standard containers may be accepted as well. The implementation could further be improved by keeping track of a maximum size method, which would make it possible to set the capacity of the vector before elements are pushed to it.

## Range combiners
Range combiners take in a variable set of ranges and output a new one that is a combination of each. The following range combiners are implemented thusfar
* join
* zip
* product

### Join
`join` takes in a variable number of ranges and outputs them in consecutive order.

```cpp
int main() {

	auto x = linrange(1, 4);
	auto y = linrange(4, 6);
	auto z = linrange(6, 9);
	
	for (auto v : join(x, y, z))
	{
		std::cout << v << std::endl;
	}
	
    return 0;
}
```
```
output:
1
2
3
4
5
6
7
8
```

### Zip
`zip` takes in a variable number of ranges and outputs a tuple containing the nth element of each range. A neat feature of the implementation is that the ranges need not be of the same length, viz

```cpp
int main() {

	auto x = linrange(1, 4);
	auto y = linrange(2, 9);
	auto z = linrange(3, 8);
	
	for (auto [a, b, c] : zip(x, y, z))
	{
		std::cout << a << "  " << b << "  " << c << std::endl;
	}
	
    return 0;
}
```
```
output:
1  2  3
2  3  4
3  4  5
```


### Product
`product` takes in a variable number of arguments and outputs the product range containing all combinations, viz
```cpp
int main() {

	auto x = linrange(1, 3);
	auto y = linrange(2, 4);
	auto z = linrange(3, 5);
	
	for (auto [a, b, c] : product(x, y, z))
	{
		std::cout << a << "  " << b << "  " << c << std::endl;
	}
	
    return 0;
}
```
```
output:
1  2  3
2  2  3
1  3  3
2  3  3
1  2  4
2  2  4
1  3  4
2  3  4
```

## Implementing custom ranges
Implementation of custom ranges is very simple. For example, the `linrange<T>` has the following implementation
```cpp
template <typename T>
struct linrange : next_interface<linrange<T>> {

	const T first;
	const T last;
	T current;

	linrange(const T& first, const T& last) : first(first), last(last), current(first) {}
	
	using type = linrange<T>;
	using value_type = T;

	void initialize()
	{
		current = first;
	}

	std::optional<value_type> next() {
		if (current != last)
			return current++;
		else
			return std::nullopt;
	}
};
```
The base class `next_interface<...>` implements an input iterator and has a `begin()` and `end()` method. A custom range should inherit from the base class and implement an `initialize()` and a `next()` method. The iterator uses the `next()` method to increment the iterator. In the process the next value is cached inside the iterator and can be dereferenced at any time without additional computations. This avoids the issue explained in [fluentcpp](https://www.fluentcpp.com/2019/02/15/how-smart-output-iterators-fare-with-the-terrible-problem-of-incrementing-a-smart-iterator/).


## Todo
The following features are still to be implemented
* conformation to the  [std::ranges](https://en.cppreference.com/w/cpp/ranges) concepts
* compatibility with [std::algorithm](https://en.cppreference.com/w/cpp/algorithm)
* more ranges covering more of the functionality of [std::ranges](https://en.cppreference.com/w/cpp/ranges)
* compile-time range types providing support for multidimensional algorithms
