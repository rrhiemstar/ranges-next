from conans import ConanFile, CMake

class ConanPackage(ConanFile):
    name = "ranges-next"
    version = "0.2.0"

    # Optional metadata
    license = "MIT"
    author = "Rene Hiemstra (rrhiemstar@gmail.com)"
    url = "https://gitlab.com/rrhiemstar/ranges-next.git"
    description = "C++20 ranges library with functional style procedures and support for easy implementation of custom ranges."
    topics = ("ranges", "iterators", "c++20")

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "include/*"
    generators = "cmake_find_package"

    no_copy_source = True

    def requirements(self):
        self.requires("gtest/cci.20210126")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.install()

    def package_info(self):
        self.info.header_only()