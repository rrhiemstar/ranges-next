#include "next/meta.hpp"
#include "test.h"


using namespace meta;

namespace meta_test {

    // testing of different callable objects

    // regular function
    bool func(int a, int b) { return a == b; };

    // lambda
    auto lambda = [](int a, int b) { return a == b; };

    // function object
    struct func_obj {
        bool operator() (int a, int b) { return a == b; };
    };

    // function object embedded in std::function
    std::function<bool(int, int)> std_func = [](int a, int b) { return a == b; };


    TEST(Meta, Function) {

        EXPECT_TRUE((Function<decltype(func)>));
        EXPECT_FALSE((Function<decltype(lambda)>));
        EXPECT_FALSE((Function<decltype(std_func)>));
        EXPECT_FALSE((Function<func_obj>));
    }

    TEST(Meta, FunctionObject) {

        EXPECT_FALSE((FunctionObject<decltype(func)>));
        EXPECT_TRUE((FunctionObject<decltype(lambda)>));
        EXPECT_TRUE((FunctionObject<decltype(std_func)>));
        EXPECT_TRUE((FunctionObject<func_obj>));
    }

    TEST(Meta, Callable) {

        EXPECT_TRUE((Callable<decltype(func)>));
        EXPECT_TRUE((Callable<decltype(lambda)>));
        EXPECT_TRUE((Callable<decltype(std_func)>));
        EXPECT_TRUE((Callable<func_obj>));
    }

    TEST(Meta, Predicate) {

        EXPECT_TRUE((Predicate<decltype(func)>));
        EXPECT_TRUE((Predicate<decltype(lambda)>));
        EXPECT_TRUE((Predicate<decltype(std_func)>));
        EXPECT_TRUE((Predicate<func_obj>));
    }

    TEST(Meta, return_type) {

        EXPECT_TRUE((std::is_same<Signature<decltype(func)>::return_arg_t, bool>::value));
        EXPECT_TRUE((std::is_same<Signature<decltype(lambda)>::return_arg_t, bool>::value));
        EXPECT_TRUE((std::is_same<Signature<decltype(std_func)>::return_arg_t, bool>::value));
        EXPECT_TRUE((std::is_same<Signature<func_obj>::return_arg_t, bool>::value));
    }

    TEST(Meta, input_type) {

        EXPECT_TRUE((std::is_same<Signature<decltype(func)>::input_arg_t, std::tuple<int,int>>::value));
        EXPECT_TRUE((std::is_same<Signature<decltype(lambda)>::input_arg_t, std::tuple<int, int>>::value));
        EXPECT_TRUE((std::is_same<Signature<decltype(std_func)>::input_arg_t, std::tuple<int, int>>::value));
        EXPECT_TRUE((std::is_same<Signature<func_obj>::input_arg_t, std::tuple<int, int>>::value));
    }

    TEST(Meta, input_arg_count) {

        EXPECT_EQ(Signature<decltype(func)>::nargs, 2);
        EXPECT_EQ(Signature<decltype(lambda)>::nargs, 2);
        EXPECT_EQ(Signature<decltype(std_func)>::nargs, 2);
        EXPECT_EQ(Signature<func_obj>::nargs, 2);
    }
}