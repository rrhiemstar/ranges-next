#include "next/next.hpp"

#include "test.h"

using namespace next;

namespace next_test {

    TEST(to_vector, lvalues) {
        auto x = linrange(1, 5);

        auto result = x | to_vector();

        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 1);
        EXPECT_EQ(result[1], 2);
        EXPECT_EQ(result[2], 3);
        EXPECT_EQ(result[3], 4);
    }

    TEST(to_vector, rvalues) {
      
      auto result = linrange(1, 5) | to_vector();

      EXPECT_EQ(result.size(), 4);
      EXPECT_EQ(result[0], 1);
      EXPECT_EQ(result[1], 2);
      EXPECT_EQ(result[2], 3);
      EXPECT_EQ(result[3], 4);
    }

    TEST(to_vector, as_function) {

      auto result = to_vector(linrange(1, 5));

      EXPECT_EQ(result.size(), 4);
      EXPECT_EQ(result[0], 1);
      EXPECT_EQ(result[1], 2);
      EXPECT_EQ(result[2], 3);
      EXPECT_EQ(result[3], 4);
    }

    TEST(take, lvalues) {
        auto x = linrange(1, 10);
        auto y = take_range(x, 4);

        auto result = y | to_vector();

        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 1);
        EXPECT_EQ(result[1], 2);
        EXPECT_EQ(result[2], 3);
        EXPECT_EQ(result[3], 4);
    }

    TEST(take, rvalues) {
        EXPECT_NO_THROW( linrange(1, 10) | take(4) );
    }

    TEST(drop, lvalues) {
        auto x = linrange(1, 10);
        auto y = x | drop(5);

        auto result = y | to_vector();

        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 6);
        EXPECT_EQ(result[1], 7);
        EXPECT_EQ(result[2], 8);
        EXPECT_EQ(result[3], 9);
    }
    
    TEST(drop, rvalues) { 
        EXPECT_NO_THROW( linrange(1, 10) | drop(4) ); 
    }

    TEST(take_while, lvalues) {

        auto x = linrange(1, 10);
        auto y = x | take_while([](int i) { return i * i + 1 <= 10; });

        auto result = y | to_vector();

        EXPECT_EQ(result.size(), 3);
        EXPECT_EQ(result[0], 1);
        EXPECT_EQ(result[1], 2);
        EXPECT_EQ(result[2], 3);
    }
    
    TEST(take_while, rvalues) { 
        EXPECT_NO_THROW( linrange(1, 10) | take_while([](int i) { return i * i + 1 <= 10; }) ); 
    }

    TEST(drop_while, lvalues) {

        auto x = linrange(1, 10);
        auto y = x | drop_while([](int i) { return i < 6; });

        auto result = y | to_vector();

        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 6);
        EXPECT_EQ(result[1], 7);
        EXPECT_EQ(result[2], 8);
        EXPECT_EQ(result[3], 9);
    }

    TEST(drop_while, rvalues) {
        EXPECT_NO_THROW(linrange(1, 10) | drop_while([](int i) { return i < 6; }));
    }
	
    TEST(filter, lvalues) {
        
        auto x = linrange(1, 10);
        auto y = x | filter([](int i) { return i % 2 == 0; });

        auto result = y | to_vector();

        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 2);
        EXPECT_EQ(result[1], 4);
        EXPECT_EQ(result[2], 6);
        EXPECT_EQ(result[3], 8);
    }
    
    TEST(filter, rvalues) {
        EXPECT_NO_THROW(linrange(1, 10) | filter([](int i) { return i % 2 == 0; }));
    }

    TEST(transform, lvalues) {
        auto x = linrange(1, 5);
        auto y = x | transform([](int i) { return i * i; });
        auto result = y | to_vector();

        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 1);
        EXPECT_EQ(result[1], 4);
        EXPECT_EQ(result[2], 9);
        EXPECT_EQ(result[3], 16);
    }
	
    TEST(transform, rvalues) {
        EXPECT_NO_THROW( linrange(1, 5) | transform([](int i) { return i * i; }) );
    }

    TEST(composition, filter_transform_take) {
        auto x = linrange(1, 10) |
            filter([](int i) { return i % 2 == 0; }) |
            transform([](int i) { return i + 1; }) |
            take(3);

        auto result = x | to_vector();

        EXPECT_EQ(result.size(), 3);
        EXPECT_EQ(result[0], 3);
        EXPECT_EQ(result[1], 5);
        EXPECT_EQ(result[2], 7);
    }


    TEST(input_range, vector) {

        std::vector<int> x{1, 2, 3, 4};

        auto result = x | filter([](int i) { return i % 2 == 0; }) | to_vector();

        EXPECT_EQ(result.size(), 2);
        EXPECT_EQ(result[0], 2);
        EXPECT_EQ(result[1], 4);
    }


    TEST(enumerate, lvalues) {
        auto x = linrange(1, 5);

        std::vector<int> count;
        std::vector<int> result;

        for (auto [i,v] : enumerate(x))
        {
            count.push_back(i);
            result.push_back(v);
        }
        
        EXPECT_EQ(count.size(), 4);
        EXPECT_EQ(count[0], 0);
        EXPECT_EQ(count[1], 1);
        EXPECT_EQ(count[2], 2);
        EXPECT_EQ(count[3], 3);
        
        EXPECT_EQ(result.size(), 4);
        EXPECT_EQ(result[0], 1);
        EXPECT_EQ(result[1], 2);
        EXPECT_EQ(result[2], 3);
        EXPECT_EQ(result[3], 4);
    }

    TEST(enumerate, rvalues) {
      EXPECT_NO_THROW(enumerate(linrange(1, 10)));
    }

    TEST(join, lvalues) {

        auto x = linrange(1, 4);
        auto y = linrange(4, 6);

        std::vector<int> collection;

        for (auto xi : join(x, y))
        {
            collection.push_back(xi);
        }

        EXPECT_EQ(collection.size(), 5);
        EXPECT_EQ(collection[0], 1);
        EXPECT_EQ(collection[1], 2);
        EXPECT_EQ(collection[2], 3);
        EXPECT_EQ(collection[3], 4);
        EXPECT_EQ(collection[4], 5);
    }

    TEST(join, rvalues) {
        EXPECT_NO_THROW( join(linrange(1, 4), linrange(4, 6)) );
    }

    TEST(join, three_inputs) {
    	
        auto x = linrange(1, 4);
        auto y = linrange(4, 6);
        auto z = linrange(6, 11);

        std::vector<int> collection;

        for (auto xi : join(x, y, z))
        {
            collection.push_back(xi);
        }

        EXPECT_EQ(collection.size(), 10);
        EXPECT_EQ(collection[0], 1);
        EXPECT_EQ(collection[1], 2);
        EXPECT_EQ(collection[2], 3);
        EXPECT_EQ(collection[3], 4);
        EXPECT_EQ(collection[4], 5);
        EXPECT_EQ(collection[5], 6);
        EXPECT_EQ(collection[6], 7);
        EXPECT_EQ(collection[7], 8);
        EXPECT_EQ(collection[8], 9);
        EXPECT_EQ(collection[9], 10);
    }
	
    TEST(zip, lvalues) {
        auto x = linrange(1, 5);
        auto y = linrange(2, 6);
        
        std::vector<int> x_collection;
        std::vector<int> y_collection;

        for (auto [xi, yi] : zip(x, y))
        {
            x_collection.push_back(xi);
            y_collection.push_back(yi);
        }

        EXPECT_EQ(x_collection.size(), 4);
        EXPECT_EQ(x_collection[0], 1);
        EXPECT_EQ(x_collection[1], 2);
        EXPECT_EQ(x_collection[2], 3);
        EXPECT_EQ(x_collection[3], 4);

        EXPECT_EQ(y_collection.size(), 4);
        EXPECT_EQ(y_collection[0], 2);
        EXPECT_EQ(y_collection[1], 3);
        EXPECT_EQ(y_collection[2], 4);
        EXPECT_EQ(y_collection[3], 5);
    }

    TEST(zip, rvalues) {
        EXPECT_NO_THROW( zip(linrange(1, 5), linrange(2, 6)) );
    }

    TEST(zip, three_inputs) {
        auto x = linrange(1, 5);
        auto y = linrange(2, 6);
        auto z = linrange(3, 7);

        std::vector<int> x_collection;
        std::vector<int> y_collection;
        std::vector<int> z_collection;

        for (auto [xi, yi, zi] : zip(x, y, z))
        {
            x_collection.push_back(xi);
            y_collection.push_back(yi);
            z_collection.push_back(zi);
        }

        EXPECT_EQ(x_collection.size(), 4);
        EXPECT_EQ(x_collection[0], 1);
        EXPECT_EQ(x_collection[1], 2);
        EXPECT_EQ(x_collection[2], 3);
        EXPECT_EQ(x_collection[3], 4);

        EXPECT_EQ(y_collection.size(), 4);
        EXPECT_EQ(y_collection[0], 2);
        EXPECT_EQ(y_collection[1], 3);
        EXPECT_EQ(y_collection[2], 4);
        EXPECT_EQ(y_collection[3], 5);
        
        EXPECT_EQ(z_collection.size(), 4);
        EXPECT_EQ(z_collection[0], 3);
        EXPECT_EQ(z_collection[1], 4);
        EXPECT_EQ(z_collection[2], 5);
        EXPECT_EQ(z_collection[3], 6);
    }
    
    TEST(zip, uneven_length) {
        auto x = linrange(1, 3);
        auto y = linrange(2, 6);

        std::vector<int> x_collection;
        std::vector<int> y_collection;

        for (auto [xi, yi] : zip(x, y))
        {
            x_collection.push_back(xi);
            y_collection.push_back(yi);
        }

        EXPECT_EQ(x_collection.size(), 2);
        EXPECT_EQ(x_collection[0], 1);
        EXPECT_EQ(x_collection[1], 2);

        EXPECT_EQ(y_collection.size(), 2);
        EXPECT_EQ(y_collection[0], 2);
        EXPECT_EQ(y_collection[1], 3);
    }

    TEST(product, lvalues) {

        auto x = linrange(1, 3);
        auto y = linrange(2, 5);

        std::vector<int> x_collection;
        std::vector<int> y_collection;

        for (auto [xi, yi] : product(x, y))
        {
            x_collection.push_back(xi);
            y_collection.push_back(yi);
        }

        EXPECT_EQ(x_collection.size(), 6);
        EXPECT_EQ(x_collection[0], 1);
        EXPECT_EQ(x_collection[1], 2);
        EXPECT_EQ(x_collection[2], 1);
        EXPECT_EQ(x_collection[3], 2);
        EXPECT_EQ(x_collection[4], 1);
        EXPECT_EQ(x_collection[5], 2);
        
        EXPECT_EQ(y_collection.size(), 6);
        EXPECT_EQ(y_collection[0], 2);
        EXPECT_EQ(y_collection[1], 2);
        EXPECT_EQ(y_collection[2], 3);
        EXPECT_EQ(y_collection[3], 3);
        EXPECT_EQ(y_collection[4], 4);
        EXPECT_EQ(y_collection[5], 4);
        
    }
	
    TEST(product, rvalues) {
      EXPECT_NO_THROW( product(linrange(1, 5), linrange(2, 6)));
    }

    TEST(product, three_inputs) {
    	
        auto x = linrange(1, 3);
        auto y = linrange(2, 5);
        auto z = linrange(3, 5);

        std::vector<int> x_collection;
        std::vector<int> y_collection;
        std::vector<int> z_collection;

        for (auto [xi, yi, zi] : product(x, y, z))
	    {
	        x_collection.push_back(xi);
	        y_collection.push_back(yi);
	        z_collection.push_back(zi);
	    }

	    EXPECT_EQ(x_collection.size(), 12);
	    EXPECT_EQ(x_collection[0], 1);
	    EXPECT_EQ(x_collection[1], 2);
	    EXPECT_EQ(x_collection[2], 1);
	    EXPECT_EQ(x_collection[3], 2);
	    EXPECT_EQ(x_collection[4], 1);
	    EXPECT_EQ(x_collection[5], 2);
	    EXPECT_EQ(x_collection[6], 1);
	    EXPECT_EQ(x_collection[7], 2);
	    EXPECT_EQ(x_collection[8], 1);
	    EXPECT_EQ(x_collection[9], 2);
	    EXPECT_EQ(x_collection[10], 1);
	    EXPECT_EQ(x_collection[11], 2);

	    EXPECT_EQ(y_collection.size(), 12);
	    EXPECT_EQ(y_collection[0], 2);
	    EXPECT_EQ(y_collection[1], 2);
	    EXPECT_EQ(y_collection[2], 3);
	    EXPECT_EQ(y_collection[3], 3);
	    EXPECT_EQ(y_collection[4], 4);
	    EXPECT_EQ(y_collection[5], 4);
	    EXPECT_EQ(y_collection[6], 2);
	    EXPECT_EQ(y_collection[7], 2);
	    EXPECT_EQ(y_collection[8], 3);
	    EXPECT_EQ(y_collection[9], 3);
	    EXPECT_EQ(y_collection[10], 4);
	    EXPECT_EQ(y_collection[11], 4);

        EXPECT_EQ(z_collection.size(), 12);
        EXPECT_EQ(z_collection[0], 3);
        EXPECT_EQ(z_collection[1], 3);
        EXPECT_EQ(z_collection[2], 3);
        EXPECT_EQ(z_collection[3], 3);
        EXPECT_EQ(z_collection[4], 3);
        EXPECT_EQ(z_collection[5], 3);
        EXPECT_EQ(z_collection[6], 4);
        EXPECT_EQ(z_collection[7], 4);
        EXPECT_EQ(z_collection[8], 4);
        EXPECT_EQ(z_collection[9], 4);
        EXPECT_EQ(z_collection[10], 4);
        EXPECT_EQ(z_collection[11], 4);
    }

    TEST(product, four_inputs) {

        auto x = linrange(1, 3);
        auto y = linrange(2, 4);
        auto z = linrange(3, 5);
        auto o = linrange(4, 6);

        std::vector<int> x_collection;
        std::vector<int> y_collection;
        std::vector<int> z_collection;
        std::vector<int> o_collection;

        for (auto [xi, yi, zi, oi] : product(x, y, z, o))
        {
            x_collection.push_back(xi);
            y_collection.push_back(yi);
            z_collection.push_back(zi);
            o_collection.push_back(oi);
        }
        
        EXPECT_EQ(x_collection.size(), 16);
        EXPECT_EQ(x_collection[0], 1);
        EXPECT_EQ(x_collection[1], 2);
        EXPECT_EQ(x_collection[2], 1);
        EXPECT_EQ(x_collection[3], 2);
        EXPECT_EQ(x_collection[4], 1);
        EXPECT_EQ(x_collection[5], 2);
        EXPECT_EQ(x_collection[6], 1);
        EXPECT_EQ(x_collection[7], 2);
        EXPECT_EQ(x_collection[8], 1);
        EXPECT_EQ(x_collection[9], 2);
        EXPECT_EQ(x_collection[10], 1);
        EXPECT_EQ(x_collection[11], 2);
        EXPECT_EQ(x_collection[12], 1);
        EXPECT_EQ(x_collection[13], 2);
        EXPECT_EQ(x_collection[14], 1);
        EXPECT_EQ(x_collection[15], 2);

        EXPECT_EQ(y_collection.size(), 16);
        EXPECT_EQ(y_collection[0], 2);
        EXPECT_EQ(y_collection[1], 2);
        EXPECT_EQ(y_collection[2], 3);
        EXPECT_EQ(y_collection[3], 3);
        EXPECT_EQ(y_collection[4], 2);
        EXPECT_EQ(y_collection[5], 2);
        EXPECT_EQ(y_collection[6], 3);
        EXPECT_EQ(y_collection[7], 3);
        EXPECT_EQ(y_collection[8], 2);
        EXPECT_EQ(y_collection[9], 2);
        EXPECT_EQ(y_collection[10], 3);
        EXPECT_EQ(y_collection[11], 3);
        EXPECT_EQ(y_collection[12], 2);
        EXPECT_EQ(y_collection[13], 2);
        EXPECT_EQ(y_collection[14], 3);
        EXPECT_EQ(y_collection[15], 3);

        EXPECT_EQ(z_collection.size(), 16);
        EXPECT_EQ(z_collection[0], 3);
        EXPECT_EQ(z_collection[1], 3);
        EXPECT_EQ(z_collection[2], 3);
        EXPECT_EQ(z_collection[3], 3);
        EXPECT_EQ(z_collection[4], 4);
        EXPECT_EQ(z_collection[5], 4);
        EXPECT_EQ(z_collection[6], 4);
        EXPECT_EQ(z_collection[7], 4);
        EXPECT_EQ(z_collection[8], 3);
        EXPECT_EQ(z_collection[9], 3);
        EXPECT_EQ(z_collection[10], 3);
        EXPECT_EQ(z_collection[11], 3);
        EXPECT_EQ(z_collection[12], 4);
        EXPECT_EQ(z_collection[13], 4);
        EXPECT_EQ(z_collection[14], 4);
        EXPECT_EQ(z_collection[15], 4);

        EXPECT_EQ(o_collection.size(), 16);
        EXPECT_EQ(o_collection[0], 4);
        EXPECT_EQ(o_collection[1], 4);
        EXPECT_EQ(o_collection[2], 4);
        EXPECT_EQ(o_collection[3], 4);
        EXPECT_EQ(o_collection[4], 4);
        EXPECT_EQ(o_collection[5], 4);
        EXPECT_EQ(o_collection[6], 4);
        EXPECT_EQ(o_collection[7], 4);
        EXPECT_EQ(o_collection[8], 5);
        EXPECT_EQ(o_collection[9], 5);
        EXPECT_EQ(o_collection[10], 5);
        EXPECT_EQ(o_collection[11], 5);
        EXPECT_EQ(o_collection[12], 5);
        EXPECT_EQ(o_collection[13], 5);
        EXPECT_EQ(o_collection[14], 5);
        EXPECT_EQ(o_collection[15], 5);
    }

	
}